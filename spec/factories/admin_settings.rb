FactoryGirl.define do
  factory :settings, class: 'Admin::Setting' do
    app_title 'SomeApp'
    admin_email_address 'admin@secret.com'
    user_activation_tokens 2
    app_imprint ''
    app_privacy_policy ''
    captcha_system 'recaptcha'
    expiry_days 30
    max_images_per_registered_user_map 3
    max_images_per_guest_user_map 2
  end
end
