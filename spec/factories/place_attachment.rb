FactoryGirl.define do
  factory :place_attachment do
    image Rack::Test::UploadedFile.new(Rails.root.join('app/assets/images/ratmap_logo.jpg'))
    place
  end
end
