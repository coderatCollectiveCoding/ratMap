describe PlaceAttachment do
  context 'Associations' do
    it { is_expected.to belong_to :place }
  end

  before do
    @place = create :place, name: 'New place'
    @image = create :place_attachment, place_id: @place.id
  end

  it 'can be created' do
    expect(@image).to be_valid
    expect(@place.reload.images[0]).to include('ratmap_logo.jpg')
  end

  it 'will be deleted with parent place' do
    expect { @place.reload.destroy }.to change(PlaceAttachment, :count).by(-1)
  end
end
