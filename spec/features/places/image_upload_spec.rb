feature 'Image upload', :js do
  before do
    # @map = create :map, :full_public, user: nil, max_images_per_place: 2
    # @place = create :place, map: @map, name: 'Foo'
    #
    # visit map_path(map_token: @map.secret_token)
    # add_place_manually
    # fill_in_valid_place_information(name: 'Place with image')
    #
    # upload_two_images
    # expect(page).to have_css("img[src*='cat.png']")
    # expect(page).to have_css("img[src*='feminism.png']")
  end

  scenario 'can upload images' do
    skip 'Flickering'
    open_place_modal
    change_one_image
    expect(page).to have_css("img[src*='dog.png']")
    expect(page).to have_css("img[src*='feminism.png']")

    open_place_modal
    destroy_one_image
    expect(page).to have_css("img[src*='dog.png']")
    expect(page).to_not have_css("img[src*='feminism.png']")
  end

  scenario 'allows reducing image limit even if max number of images is present' do
    skip 'Flickering'
    @map.update_attributes(max_images_per_place: 1)
    open_place_modal
    give_error_message_when_changing_image

    destroy_one_image
    open_place_modal
    change_one_image
    expect(page).to have_css("img[src*='dog.png']")
  end

  private

  def give_error_message_when_changing_image
    upload_file(0, 'dog.png')
    find('.submit-place-button').click
    expect(page).to have_css('.alert-danger')
  end

  def destroy_one_image
    find_all('.destroy-image').first.click
    find('.submit-place-button').click
    expect(page).to have_content('Changes saved')
    expect(page).to_not have_content('Images')
    find('.leaflet-marker-icon').click
  end

  def change_one_image
    upload_file(0, 'dog.png')
    find('.submit-place-button').click
    expect(page).to have_content('Changes saved')
    expect(page).to_not have_content('Images')
    find('.leaflet-marker-icon').click
  end

  def upload_two_images
    upload_file(0, 'cat.png')
    upload_file(1, 'feminism.png')
    find('.submit-place-button').click
    expect(page).to have_content('Successfully created')
    expect(page).to_not have_content('Images')
    find('.leaflet-marker-icon').click
  end

  def open_place_modal
    sleep 0.5 # modal does not want to open without sleep time
    find('.edit-place').click
    expect(page).to have_content('Images')
  end

  def upload_file(image_number, file_name)
    binding.pry
    attach_file(
      "place_place_attachments_attributes_#{image_number}_image",
      File.join(Rails.root + "spec/support/images/#{file_name}")
    ).click
  end
end
