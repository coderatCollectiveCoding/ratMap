class AddMaxImageCountPerPlaceToMaps < ActiveRecord::Migration
  def change
    add_column :maps, :max_images_per_place, :integer, null: false, default: 3
  end
end
