class AddMaxImagesPerGuestUserMapToAdminSettings < ActiveRecord::Migration
  def change
    add_column :admin_settings, :max_images_per_guest_user_map, :integer, default: 0, null: false
  end
end
