namespace :db do
  namespace :seed do
    desc 'Creates an admin user. bundle exec rake db:seed:admin_user admin@test.com password'
    task admin_user: :environment do
      ARGV.each { |a| task a.to_sym do ; end }
      User.create!(is_admin: true, name: 'App admin', email: ARGV[1], password:ARGV[2], password_confirmation:ARGV[2])
    end
  end
end
