#! /bin/sh
bundle exec rake db:exists && bundle exec rake db:migrate || bundle exec rake db:setup
bundle exec rake assets:precompile

rm -rf /ratmap/tmp/pids/server.pid
exec "$@"
