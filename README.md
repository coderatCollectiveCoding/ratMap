# **RatMap**
[![Build Status](https://gitlab.com/coderatCollektivCoding/ratMap/badges/master/pipeline.svg)](https://gitlab.com/coderatCollektivCoding/ratMap/badges/master/pipeline.svg)
[![Test Coverage](https://gitlab.com/coderatCollektivCoding/ratMap/badges/master/coverage.svg)](https://gitlab.com/coderatCollektivCoding/ratMap/badges/master/coverage.svg)

ratMap is a free OpenSource, self-hosted Ruby on Rails 4 application for collaboratively mapping and sharing points of interest (POI) on a website based on [OpenStreetMap](https://www.openstreetmap.org/about). Users with or without accounts can create maps with an entirely custom contextual scope by configuring a set of categories attachable to every POI you insert. So for instance you can realise mapping...

* the most favorite restaurants within a peer group of yours
* recommendable doctors
* the local distributen of public welfare in your area
* the most propable places to find a goblin

Check out our running instance under https://maps.coderat.cc or host one yourself!

Please note that this software is the outcome of a Ruby on Rails / JS learning project. Hence you're might gonna find lots of smelly code and bad programming patterns all over the places. Regard this piece of software as a feasability study / public beta of a good idea that we might come back to one day. Hence ratMap is sparsely maintained right now. Nevertheless, if you have a feature request please do not hesitate to open up an issue [here](https://gitlab.com/coderatCollektivCoding/ratMap/-/issues) or contribute a feature via a merge request.

## **Basic Features**
* **Individual maps**: Create _public_, _semi-public_ or _private maps_ as a registered user or guest
* **Share maps**: Invite others to see what you've contributed or embed your map on other websites
* **Reviewing contributions**: Let guests contribute POIs on public maps and either accept or revert entries on your map being its administrator
* **Twitter support**: Post a short twitter notification if a new POI has been inserted on your map (experimental!)

## **Installation**
### **Docker installation** (recommended)
ratMap is powered by Docker and uses pre-built images that contain every component of the software. In order to run ratmap in production you need to have [Docker](https://docs.docker.com/engine/install/ubuntu/) and [docker-compose](https://docs.docker.com/compose/install/) up and running on your system.

You can find the entirety of ENV variables you need to specify under `sample.env` (DB, Mail, captcha, etc.) before running the app.

A simple `docker-compose up -d` should pull all the docker images necessary to run the application. You can specify the port under which your app will be available within the docker-compose file.

### **Development**
You can either develop within a docker environment or via starting the development server in your local shell environment via `rails server`. If you want to contribute to the project please take care to have a PostgreSQL server running on your development machine and to install the following software, otherwise testing might not work properly

  * Native binaries for [QT](https://www.qt.io/) (qt4-dev-tools, libqt4-dev, libqt4-core libqt4-gui)
  * A Javascript framework like [nodejs](https://nodejs.org/)

  Under Debian based systems (Debian, Ubuntu, etc.) you can install everything via
	```
	sudo apt-get install qt4-dev-tools libqt4-dev libqt4-core libqt4-gui nodejs
	```

## **Application configuration**
Check out this [wiki page](https://github.com/magdalena19/lberg-map/wiki/Application-configuration) for further information on ratMap configuration.

## **Configure Nginx/SSL, Backup the database**
* [Nginx/SSL](https://github.com/magdalena19/ratMap/wiki/Nginx-SSL-Sample-Config)
* [Database Backup](https://github.com/magdalena19/ratMap/wiki/Database-Backup)
