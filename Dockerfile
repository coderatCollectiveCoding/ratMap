# FROM ruby:2.4-alpine
FROM ruby:2.4
MAINTAINER info@coderat.cc

# RUN apk add --update build-base postgresql-dev tzdata yarn nodejs imagemagick ghostscript git firefox-esr
RUN apt update && apt install -y libpq-dev tzdata yarn nodejs imagemagick ghostscript git firefox-esr

RUN mkdir /ratmap
WORKDIR /ratmap
COPY . /ratmap

RUN gem install bundler:1.17.3 bundler-audit
RUN bundle install

# RUN yarn global add geckodriver
RUN bundle exec rake assets:precompile
