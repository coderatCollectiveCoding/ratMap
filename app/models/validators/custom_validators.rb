module CustomValidators
  class PhoneNumberFormatValidator < ActiveModel::EachValidator
    PHONE_REGEX = /\A((?![a-zA-Z]).){3,20}\z/

    def validate_each(record, attribute, value)
      unless value =~ PHONE_REGEX
        record.errors[attribute] << I18n.t('phone_number.formats.invalid')
      end
    end
  end

  class UrlFormatValidator < ActiveModel::EachValidator
    URL_REGEX = %r[\A​(https?:\/\/)?(www\.)[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,4}\b([-a-zA-Z0-9@:%_\+.~#?&\/=]*)|(https?:\/\/)?(www\.)?(?!ww)[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,4}\b([-a-zA-Z0-9@:%_\+.~#?&\/=]*)\z]

    def validate_each(record, attribute, value)
      unless value =~ URL_REGEX
        record.errors[attribute] << I18n.t('url.formats.invalid')
      end
    end
  end

  class EmailFormatValidator < ActiveModel::EachValidator
    EMAIL_REGEX = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/

    def validate_each(record, attribute, value)
      unless value =~ EMAIL_REGEX
        record.errors[attribute] << I18n.t('email_address.formats.invalid')
      end
    end
  end

  class IsBase64Validator < ActiveModel::EachValidator
    def validate_each(record, attribute, value)
      unless base64?(value)
        record.errors[attribute] << I18n.t('not_base64_encoded')
      end
    end

    private

    def base64?(string)
      string.split('-').map do |token_part|
        token_part.is_a?(String) && Base64.encode64(Base64.decode64(token_part)) == token_part
      end.all?
    end
  end

  class RespectQuotaValidator < ActiveModel::EachValidator # :nodoc:
    attr_reader :record

    def initialize(options)
      super
    end

    def validate_each(record, attribute, value)
      @record = record

      value_length = value.respond_to?(:length) ? value.length : value.to_s.length

      # Handle quota cases separately (per place + per map)
      if !value.nil? && !record.map.nil?
        # max_images_per_place
        unless value_length <= record.map.max_images_per_place
          record.errors.add(:base, I18n.t('errors.max_images_per_place_exceeded'))
        end

        # max_images_per_map
        unless record.map.place_attachments.count <= max_map_images_quota
          record.errors.add(:base, I18n.t('errors.max_images_per_map_exceeded'))
        end
      end
    end

    private

    def max_map_images_quota
      record.map.guest_map? ?
        Admin::Setting.current_setting.max_images_per_guest_user_map :
        Admin::Setting.current_setting.max_images_per_registered_user_map
    end
  end
end
