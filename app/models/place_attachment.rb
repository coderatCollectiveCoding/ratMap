class PlaceAttachment < ActiveRecord::Base
   mount_uploader :image, ImageUploader
   belongs_to :place

   # validate :max_images_per_place_not_exceeded
   # validate :max_images_per_map_not_exceeded

   # def max_images_per_place_not_exceeded
   #   return false unless self.place # necessary?
   #   binding.pry
   #   if self.place.place_attachments(:reload).count > self.place.map.max_images_per_place
   #     errors.add(:base, I18n.t('activerecord.errors.models.place_attachment.exceeded_quota'))
   #   end
   # end

   # def max_images_per_map_not_exceeded
   #   return false unless self.place # necessary?
   #   self.place.map.places.each { |p| p.reload }
   #   debugger
   #   if self.place.map.place_attachments.count > self.place.map.max_images
   #     errors.add(:base, I18n.t('activerecord.errors.models.place_attachment.exceeded_map_limit'))
   #   end
   # end
end
