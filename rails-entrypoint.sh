#! /bin/sh
bundle exec rake db:exists && bundle exec rake db:migrate || bundle exec rake db:create

# Unicorn cant create this dir 
mkdir -p /ratmap/tmp/pids

rm -rf /ratmap/tmp/pids/unicorn.pid
exec "$@"
